---
layout: default
---
Il faut que je vous parle d'un truc qui me trotte dans la tête depuis pas mal d'années. Depuis trop d'années !
Il s'agit maintenant de passer de l'idée à l'action. Et pour être tout à fait honnête, si je vous en parle, c'est parce que j'ai besoin de vous :)

Voici le problème. Certains d'entre vous le savent déjà : je joue du piano. Et le problème majeur que je rencontre lorsque je suis sur scène et que je joue du piano, c'est que je ne peux pas me déplacer. Je peux vaguement jouer du piano debout, dodeliner de la tête, esquisser deux trois pas de danse, mais cela reste extrêmement limité. Ce n'est plus possible, cela doit changer !

- J'ai envie de jouer du piano et de chanter en marchant.
- J'ai envie de jouer du piano sans être coincé sur scène, pouvoir partager des moments au coeur du public, être physiquement avec les spectateurs.
- J'ai envie d'être dans un groupe où tous les musiciens ont la liberté d'avancer, de reculer, de danser.

Bref, j'ai envie de monter une fanfare. Une fanfare sans cuivre, cela va sans dire. Une fanfare qui reprendrait des morceaux connus, qui déambulerait dans les rues, où l'on ne ferait plus la distinction entre les musiciens et les spectateurs, parce que tout le monde chanterait, tout le monde danserait.

Cette fanfare, elle s'appellerait la Fanfare Kismar.

J'ai envie d'arrêter de parler au conditionnel.

J'aime faire ce qui me plait.

Cette fanfare, je souhaite qu'elle sorte de terre début juin.

Pour cela, j'ai besoin de vous !

- J'ai besoin de construire un "truc" qui puisse supporter une sono, et que l'on puisse pousser.
- J'ai besoin de pouvoir brancher un clavier léger à ce truc.
- J'ai besoin d'être harnaché à ce clavier léger.
- J'ai besoin de musiciens pour monter un répertoire de chansons connues (j'ai déjà des idées sur les musiciens)
- J'ai besoin de bricoleurs qui puissent m'aider à construire le "truc".
- J'ai besoin de quelques personnes qui aiment chanter.
- J'ai besoin de quelques personnes qui aiment danser.
- J'ai besoin de quelques personnes qui aiment marcher.
- J'ai besoin de personnes qui n'aiment pas forcément chanter, danser ou marcher, mais qui aiment l'idée de la Fanfare Kismar.

Alors, maintenant, j'ai une seule demande à te faire : coche la ou les cases qui te correspondent !

<div class="typeform-widget" data-url="https://krichtof.typeform.com/to/ca78mP" style="width: 100%; height: 500px;"></div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
