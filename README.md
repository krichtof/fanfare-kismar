# C'est quoi ?

[La fanfare Kismar](http://lafanfare.kismar.net) est une fanfare sans cuivre qui déambule en faisant chanter, danser et marcher les foules en délire.
# Pourquoi ?

La fanfare Kismar se donne comme objectif de mettre en mouvement une foule de personnes qui ne se connaissent pas, et qui font ensemble quelque chose qui leur donnent de l'énergie, tout en se réappropriant l'espace public.


# Métriques

- le dispositif technique est-il opérationnel (indicateur jusqu'à juin)
- diversité de la communauté (est-ce j'ai au moins une chanteuse, un danseur, une marcheuse, une bricoleuse, un designer)
- le pourcentage de personnes qui chantent ou dansent
- nombre de personnes lors des interventions de la fanfare

# Vie de l'équipe

2 points hebdommaidaires de 10mn max le mardi à 11h53 et le vendredi à 12h04 pour répondre à ces questions :

- qu'est-ce qui a été fait ?
- qu'est-ce qu'on compte faire ?
- quels sont les éventuels poins de blocage ?
