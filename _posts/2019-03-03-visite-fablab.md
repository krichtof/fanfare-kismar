---
layout: post
title:  "Premier rdv des bricolos Kismar"
date:   2019-01-13
---

Nous nous sommes retrouvés au petit fablab de Paris, pour faire connaissance avec leur matériel.
Andy nous a fait la visite. Voici les outils qui pourraient nous servir par la suite :
- scie à ruban
- perceuse à colonnes
- découpeuse laser
- imprimante 3D

Direction Café Colette ensuite pour remuer nos méninges, et discuter du prototype.
Débats enflammés : triporteur ou simple chariot ? Commençons simple, un chariot pourrait faire l'affaire !
[Le chariot](https://www.manomano.fr/chariot-de-jardin/chariot-de-jardin-remorque-a-main-avec-bache-cotes-amovibles-max-350kg-138078?model_id=138078) portera la table de mixage, le récepteur HF, [les enceintes](https://www.thomann.de/fr/ld_systems_maui_11_mix.htm?ref=search_prv_6), la batterie de voiture pour alimenter le tout, et eventuellement le dispositif d'affichage des paroles.

Côté [clavier](https://www.thomann.de/fr/m_audio_code_49_black.htm?sid=3af985f7d7e30b30179ab3a086731352), il sera harnaché au pianiste. On branchera dessus un raspberry pi pour générer le son qui sera envoyé en HF au chariot sono


